package pl.patryktoof.homeserver;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

@RestController
public class GreetingController {

    @GetMapping("/greeting")
    public String getGreeting(@RequestParam(required = false, defaultValue = "World") String name) {
        var os = System.getProperty("os.name");
        var ip = getHostAddress();
        return new StringBuilder("Hello " + name + "!\n")
            .append("Best regards from the machine running on ")
            .append(os)
            .append("\n")
            .append("in the host with IPv4: ")
            .append(ip)
            .toString();
    }

    private static String getHostAddress() {
        try (final var datagramSocket = new DatagramSocket()) {
            datagramSocket.connect(InetAddress.getByName("8.8.8.8"), 12345);
            return datagramSocket.getLocalAddress().getHostAddress();
        } catch (SocketException | UnknownHostException e) {
            return "No connection";
        }
    }
}
