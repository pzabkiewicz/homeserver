FROM eclipse-temurin:17-jdk-ubi9-minimal AS builder
WORKDIR /homeserver
COPY /gradle ./gradle
COPY /src ./src
COPY gradlew build.gradle.kts settings.gradle.kts ./
RUN chmod +x ./gradlew
RUN microdnf --assumeyes install findutils
RUN ./gradlew assemble

FROM eclipse-temurin:17-jre-ubi9-minimal
WORKDIR /homeserver
EXPOSE 8080
COPY --from=builder ./homeserver/build/libs/homeserver-0.0.1-SNAPSHOT.jar ./
CMD ["java", "-jar", "homeserver-0.0.1-SNAPSHOT.jar"]
